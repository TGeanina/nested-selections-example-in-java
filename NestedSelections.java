//Author: Geanina Tambaliuc

public class NestedSelections {
	
	public static void main(String[] args) {
		// defining the integer values
		int first =7;
		int second = 5;
		int third = 3;
		
		int min=0,max=0,mid=0;
		
		if(first>=second)
		{
			if(first>=third)
			{
				max=first;
			}
			else if(first<=third)
			{
				mid=first;
			}
		}
		else if(first<=second)
		{
			if(first<=third)
			{
				min=first;
			}
			else if(first>=third)
			{
				mid=first;
			}
		}
		
		if(second>=first)
		{
			if(second>=third)
			{
				max=second;
			}
			else if(second<=third)
			{
				mid=second;
			}
		}
		else if(second<=first)
		{
			if(second<=third)
			{
				min=second;
			}
			else if(second>=third)
			{
				mid=second;
			}
		}
		
		if(third>=first)
		{
			if(third>=second)
			{
				max=third;
			}
			else if(third<=second)
			{
				mid=third;
			}
		}
		else if(third<=first)
		{
			if(third<=second)
			{
				min=third;
			}
			else if(third>=second)
			{
				mid=third;
			}
		}
		
		System.out.println("Min: "+min);
		System.out.println("Mid: "+mid);
		System.out.println("Max: "+ max);
		
	}
}
